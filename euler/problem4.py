#!/usr/bin/env python

print max([j*i for j in range(100, 1000) for i in range(100, 1000) if str(j*i) == str(j*i)[::-1]])
