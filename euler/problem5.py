#!/usr/bin/env python

i=1
while True:
    if sum([i%j for j in range(1,21)]):
        i+=1
    else:
        break
print i
