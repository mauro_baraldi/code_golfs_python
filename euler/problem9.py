#!/usr/bin/env python

def solution(res):
    for a in range(1, res+1):
        for b in range(1, res+1):
            c = res - b - a
            if a * a + b * b == c * c:
                return a, b, c, a*b*c

if __name__ == '__main__':
    solution(1000)
