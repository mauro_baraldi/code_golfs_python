#!/usr/bin/env python

def fibonacci(limit):
    this, next, sum = 0, 1, 0
    while next < limit+1:
        this, next = next, this+next
        sum += this if this % 2 else 0
    return sum

