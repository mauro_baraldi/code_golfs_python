#!/usr/bin/env python

i, n = 2, 600851475143
while i * i < n:
    while n % i == 0:
        n = n / i
        i = i + 1
print (n)
